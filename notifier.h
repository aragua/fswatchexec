#ifndef FSNOTIFIER_H
#define FSNOTIFIER_H

typedef struct
{
	char *path;
	char *filter;
	int mode;
	char *file_action;
	char *dir_action;
	int wd;
	pthread_t thread;
	int quit;
	int verbose;
} notifier_t;

notifier_t *notifier_new(char *path, char *filter);
void notifier_print_available_modes(FILE *stream, char *prefix);
void notifier_set_verbosity(notifier_t *nf, int value);
int notifier_watch(notifier_t *nf);
void notifier_destroy(notifier_t *nf);

#endif /* FSNOTIFIER_H */