#define _GNU_SOURCE
#include <ctype.h>
#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <unistd.h>

#include "notifier.h"

static int inotify_fd = -1;

struct inotify_filter
{
	char *str;
	int value;
};

#define IN_MODE_NUMBER 25
static struct inotify_filter in_mode_value[IN_MODE_NUMBER] = {
	{"IN_ACCESS", 0x00000001},
	{"IN_MODIFY", 0x00000002},
	{"IN_ATTRIB", 0x00000004},
	{"IN_CLOSE_WRITE", 0x00000008},
	{"IN_CLOSE_NOWRITE", 0x00000010},
	{"IN_CLOSE", (IN_CLOSE_WRITE | IN_CLOSE_NOWRITE)},
	{"IN_OPEN", 0x00000020},
	{"IN_MOVED_FROM", 0x00000040},
	{"IN_MOVED_TO", 0x00000080},
	{"IN_MOVE", (IN_MOVED_FROM | IN_MOVED_TO)},
	{"IN_CREATE", 0x00000100},
	{"IN_DELETE", 0x00000200},
	{"IN_DELETE_SELF", 0x00000400},
	{"IN_MOVE_SELF", 0x00000800},
	{"IN_UNMOUNT", 0x00002000},
	{"IN_Q_OVERFLOW", 0x00004000},
	{"IN_IGNORED", 0x00008000},
	{"IN_ONLYDIR", 0x01000000},
	{"IN_DONT_FOLLOW", 0x02000000},
	{"IN_EXCL_UNLINK", 0x04000000},
	{"IN_MASK_CREATE", 0x10000000},
	{"IN_MASK_ADD", 0x20000000},
	{"IN_ISDIR", 0x40000000},
	{"IN_ONESHOT", 0x80000000},
	{"IN_ALL_EVENTS", (IN_ACCESS | IN_MODIFY | IN_ATTRIB | IN_CLOSE_WRITE | IN_CLOSE_NOWRITE | IN_OPEN | IN_MOVED_FROM |
					   IN_MOVED_TO | IN_CREATE | IN_DELETE | IN_DELETE_SELF | IN_MOVE_SELF)},
};

#define EVENT_SIZE	  (sizeof(struct inotify_event))
#define EVENT_BUF_LEN (1024 * (EVENT_SIZE + 16))

void notifier_print_available_modes(FILE *stream, char *prefix)
{
	for (int i = 0; i < IN_MODE_NUMBER; i++)
	{
		fprintf(stream, "%s%s\n", prefix, in_mode_value[i].str);
	}

	return;
}

static inline int get_mode_value(char *str)
{
	char *token;
	int ret = 0;
	token = strtok(str, "|");
	while (token != NULL)
	{
		for (int i = 0; i < IN_MODE_NUMBER; i++)
		{
			if (strcmp(token, in_mode_value[i].str) == 0)
			{
				ret |= in_mode_value[i].value;
			}
		}
		token = strtok(NULL, "|");
	}
	return ret;
}

static inline char *get_mode_from_value(int value)
{
	for (int i = 0; i < IN_MODE_NUMBER; i++)
	{
		if (in_mode_value[i].value == value)
			return in_mode_value[i].str;
	}
	return NULL;
}

notifier_t *notifier_new(char *path, char *filter)
{
	notifier_t *nf = NULL;

	if (inotify_fd < 0)
	{
		inotify_fd = inotify_init();
		if (inotify_fd < 0)
		{
			perror("inotify_init");
			return NULL;
		}

		nf = malloc(sizeof(notifier_t));
		if (nf)
		{
			nf->path = path;
			nf->wd = 0;
			nf->quit = 0;
			nf->filter = strtok(filter, ";");
			nf->file_action = strtok(NULL, ";");
			nf->dir_action = strtok(NULL, ";");
			nf->verbose = 0;
			if (nf->filter)
			{
				nf->mode = get_mode_value(nf->filter);
			}
			else
			{
				free(nf);
				nf = NULL;
			}
		}
	}
	return nf;
}

void notifier_set_verbosity(notifier_t *nf, int value)
{
	if (nf)
		nf->verbose = value;
}

static void notifier_show(notifier_t *nf)
{
	if (nf)
	{
		printf("Watch path: %s\n", nf->path);
		printf("mode[%s] = %d\n", nf->filter, nf->mode);
		printf("File action: %s\n", nf->file_action);
		printf("Directory action: %s\n", nf->dir_action);
		printf("fd = %d, wd = %d, quit = %d\n", inotify_fd, nf->wd, nf->quit);
	}
}

void *notifier_watch_routine(void *p)
{
	notifier_t *nf = p;
	char buffer[EVENT_BUF_LEN];

	if (nf->verbose)
		notifier_show(nf);
	nf->wd = inotify_add_watch(inotify_fd, nf->path, nf->mode);
	if (nf->wd > 0)
	{
		while (!nf->quit)
		{
			int length = 0;
			int i = 0;

			length = read(inotify_fd, buffer, EVENT_BUF_LEN);
			if (length >= 0)
			{
				if (nf->verbose)
					printf("Coming event ...\n");
				while (i < length)
				{
					struct inotify_event *event = (struct inotify_event *)&buffer[i];
					if (event->len)
					{
						if (event->mask & nf->mode)
						{
							char *launch = NULL;
							char *cmd = NULL;
							int ret = 0;
							if (event->mask & IN_ISDIR)
								cmd = nf->dir_action;
							else
								cmd = nf->file_action;
							if (!cmd || (strlen(cmd) == 0))
								cmd = "echo No action specidified for ";
							ret = asprintf(&launch, "%s %s/%s", cmd, nf->path, event->name);
							if (ret > 0)
							{
								printf("[%s]Launching %s\n", get_mode_from_value(event->mask), launch);
								ret = system(launch);
								if (nf->verbose)
									printf("Return: %d\n", ret);
								free(launch);
							}
						}
					}
					i += EVENT_SIZE + event->len;
				}
			}
			else
			{
				if (nf->verbose)
					perror("read");
				nf->quit = 1;
			}
		}
	}
	return NULL;
}

int notifier_watch(notifier_t *nf)
{
	if (nf)
	{
		int ret = 0;
		ret = pthread_create(&nf->thread, NULL, notifier_watch_routine, nf);
	}
	return 0;
}

void notifier_destroy(notifier_t *nf)
{
	if (nf)
	{
		nf->quit = 1;
		pthread_cancel(nf->thread);
		pthread_join(nf->thread, NULL);
		inotify_rm_watch(inotify_fd, nf->wd);
		free(nf);
	}
	close(inotify_fd);
}
