CC?=gcc
CFLAGS?=-g 
LDFLAGS+=-lpthread
DESTDIR?=/usr/bin

all: fswatchexec

clean:
	rm -rf fswatchexec

install:
	install -d ${DESTDIR}
	install -m 755 ./fswatchexec ${DESTDIR}/fswatchexec

fswatchexec: fswatchexec.c notifier.c
	${CC} -o $@ ${CFLAGS} $^ ${LDFLAGS}

format:
	clang-format -i *.c *.h