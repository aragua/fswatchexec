#define _GNU_SOURCE
#include <ctype.h>
#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "notifier.h"

static int verbose = 0;
static int quit_loop = 0;

void sigint_handler(int signal)
{
	if (signal == SIGINT)
	{
		if (verbose)
			printf("\nIntercepted SIGINT!\n");
		quit_loop = 1;
	}
}

void set_signal_action(void)
{
	struct sigaction act;
	bzero(&act, sizeof(act));
	act.sa_handler = &sigint_handler;
	sigaction(SIGINT, &act, NULL);
}

static void usage()
{
	fprintf(stderr, "Usage: fswatchexec -p <path> -f <filter>\n");
	fprintf(stderr, "\tpath: path to file or directory to watch\n");
	fprintf(stderr, "\tfilter: composed of action mode to watch and actions\n");
	fprintf(stderr, "\t        \"mode;action_if_regular;action_if_dir\"\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "Available modes:\n");
	notifier_print_available_modes(stderr, "\t- ");
}

int main(int argc, char **argv)
{
	int c;
	char *path = NULL;
	char *filter = NULL;
	notifier_t *nf = NULL;

	while ((c = getopt(argc, argv, "f:p:v")) != -1)
	{
		switch (c)
		{
		case 'f':
			filter = optarg;
			break;
		case 'p':
			path = optarg;
			break;
		case 'v':
			verbose = 1;
			break;
		default:
			usage();
			exit(-1);
		}
	}

	if (!path || !filter)
	{
		usage();
		exit(-1);
	}

	nf = notifier_new(path, filter);
	if (nf)
	{
		if (verbose)
			printf("Starting to watch\n");
		notifier_set_verbosity(nf, 1);
		notifier_watch(nf);

		set_signal_action();
		while (!quit_loop)
		{
			if (verbose)
				printf("Loop (quit_loop =%d)!\n", quit_loop);
			sleep(10);
		}
		notifier_destroy(nf);
	}
	else
	{
		usage();
		exit(-1);
	}

	return 0;
}